# getmeouttahere

## Example usage

`getmeouttahere --pretty --from "Utrecht Centraal" --to "Amsterdam Zuid"` will
show you the next few trains leaving from Utrecht Central Station to station
Amsterdam Zuid in a pretty formatted style.

## Description

`getmeouttahere` is a CLI-program that tells you when the trains will leave in
The Netherlands. You can pass it your departure and destination stations and
it will show you when the next train will be due to leave. It has some other
options as well, see `getmeouttahere --help` for all of them:

```none
usage: getmeouttahere [-f|--from location] [-t|--to location]
                      [-d|--date datetime] [-a|--arrival]

OPTIONS
 -a,--arrival           Date and time specified is arrival time (default:
                        departure time).
 -d,--date <arg>        The date and time in yyyy-MM-ddTHH:mm format.
 -f,--from <arg>        The departure station.
 -F,--separator <arg>   Separates result fields.
 -h,--help              Show this help text.
    --passthrough       Return response from web service directly without
                        parsing. Returns a (long) JSON string.
    --pretty            Format result in a pretty way.
 -t,--to <arg>          The arrival station.
```

Since I mainly wrote this for work when I need to see momentarily when my next
train will leave, you can set your default departure and destination stations
in `outta.properties`.

## Disclaimer

`getmeouttahere` makes use of the Dutch Railways' RESTful web service, which
is being used without explicit permission.
