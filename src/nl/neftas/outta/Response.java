package nl.neftas.outta;

import com.google.gson.*;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

public class Response {

    private static final SimpleDateFormat TO_DATE = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm");
    private static final SimpleDateFormat TO_TIME = new SimpleDateFormat("HH:mm");
    private static final TimeZone CEST = TimeZone.getTimeZone("CEST");
    private JsonObject json;
    private List<TrainRide> rides = new ArrayList<>();
    private List<Disturbance> disturbances = new ArrayList<>();
    private List<Activity> activities = new ArrayList<>();
    private ResponseLocation departureLocation;
    private ResponseLocation arrivalLocation;
    private final String nowIso8601Date;
    private String firstRideDateTime;
    private boolean isFirstRideDateTimeNull = true;

    public Response(String response) {
        nowIso8601Date = String.format("%tFT%<tR", Calendar.getInstance(CEST));
        json = new JsonParser().parse(response).getAsJsonObject();
        parseRides();
        parseArrivalLocation();
        parseDepartureLocation();
        parseDisturbances();
        parseActivities();
    }

    @Override
    public String toString() {
        return "Response{" +
                "json=" + json +
                ", rides=" + rides +
                ", departureLocation=" + departureLocation +
                ", arrivalLocation=" + arrivalLocation +
                ", nowIso8601Date='" + nowIso8601Date + '\'' +
                '}';
    }

    private void parseRides() {
        JsonArray jsonTrainRides = json.getAsJsonArray("reismogelijkheden");
        for (JsonElement trainRide : jsonTrainRides) {
            TrainRide ride = new Gson().fromJson(trainRide, TrainRide.class);
            if (isFirstRideDateTimeNull) {
                firstRideDateTime = ride.getVertrektijd();
                isFirstRideDateTimeNull = (firstRideDateTime == null);
            }
            rides.add(ride);
        }
    }

    private void parseDepartureLocation() {
        departureLocation = new Gson().fromJson(json.get("vertreklocatie"), ResponseLocation.class);
    }

    private void parseArrivalLocation() {
        arrivalLocation = new Gson().fromJson(json.get("aankomstlocatie"), ResponseLocation.class);
    }

    private void parseDisturbances() {
        JsonObject disturbancesObject = json.getAsJsonObject("storingen");
        for (Map.Entry<String, JsonElement> elem : disturbancesObject.entrySet()) {
            disturbances.add(new Gson().fromJson(elem.getValue(), Disturbance.class));
        }
    }

    private void parseActivities() {
        JsonObject activitiesObject = json.getAsJsonObject("werkzaamheden");
        for (Map.Entry<String, JsonElement> elem : activitiesObject.entrySet()) {
            activities.add(new Gson().fromJson(elem.getValue(), Activity.class));
        }
    }

    // TODO (neftas): refactor to String getPretty()
    protected String getPretty() {
        final StringBuilder result = new StringBuilder(1024);
        final StringBuilder dashedLine = new StringBuilder(64);
        for (int i = 0; i < 6; i++) {
            dashedLine.append("----------");
        }
        result.append(String.format("Reis van %s naar %s op %s\n",
                                    departureLocation.getNaam(),
                                    arrivalLocation.getNaam(),
                                    (isFirstRideDateTimeNull) ? nowIso8601Date : firstRideDateTime.substring(0, firstRideDateTime.indexOf('T'))));

        if (!disturbances.isEmpty()) {
            result.append("\n### STORINGEN ###\n");
            for (int i = 0, j = disturbances.size(); i < j; i++) {
                Disturbance currentDisturbance = disturbances.get(i);
                result.append(i+1 + ":\t" + currentDisturbance.getBerichtWithoutHTML() + "\n");
            }
            result.append("### EIND STORINGEN ###\n\n");
        }

        final String headFormat = "%-15s | %-15s | %-15s";
        final String header = dashedLine + "\n" + String.format(headFormat, "VERTREKTIJD", "AANKOMSTTIJD", "REISTIJD") + "\n" + dashedLine + "\n";
        result.append(header);

        final String rideFormat = "%-10s %-4s | %-10s %-4s | %-7s %-7s\n";
        String departureTime = null;
        String arrivalTime = null;
        for (TrainRide ride : rides) {
            try {
                departureTime = TO_TIME.format(TO_DATE.parse(ride.getVertrektijd()));
                arrivalTime = TO_TIME.format(TO_DATE.parse(ride.getAankomsttijd()));
            } catch (ParseException ex) {
                ex.printStackTrace();
                System.exit(4);
            }
            int departureDelay = 0;
            int arrivalDelay = 0;
            if (ride.getVertrektijdVertraging() != null) {
                departureDelay = ride.getVertrektijdVertraging().getMinuten();
            }
            if (ride.getAankomsttijdVertraging() != null) {
                arrivalDelay = ride.getAankomsttijdVertraging().getMinuten();
            }
            RideDuration plannedRideDuration = ride.getReistijd();
            RideDuration actualRideDuration = ride.getReistijdActueel();
            result.append(String.format(rideFormat,
                    departureTime, (departureDelay > 0) ? "+" + departureDelay : "",
                    arrivalTime, (arrivalDelay > 0) ? "+" + arrivalDelay : "",
                    plannedRideDuration, (actualRideDuration.equals(plannedRideDuration)) ? "" : actualRideDuration));
        }
        // remove last newline
        result.deleteCharAt(result.length() - 1);

        if (!activities.isEmpty()) {
            result.append("\n### WERKZAAMHEDEN ###\n");
            for (int i = 0, j = activities.size(); i < j; i++) {
                Activity currentActivity = activities.get(i);
                result.append(i+1 + ":\t" + currentActivity.getTitel() + ", ");
                result.append(currentActivity.getSubTitel() + "\n");
                result.append("\tAdvies: " + currentActivity.getAdvies() + "\n");
                result.append("\tGevolg: " + currentActivity.getGevolg() + "\n");
                result.append("\tExtra reistijd: " + currentActivity.getExtraReistijd() + "\n");
            }
        }

        return result.toString();
    }

    /**
     * Returns a easily parsable string separated by {@code sep} containing 6 fields:
     * <ol>
     * <li>departure time</li>
     * <li>departure time delay in minutes</li>
     * <li>arrival time</li>
     * <li>arrival time delay in minutes</li>
     * <li>travel time</li>
     * <li>current travel time</li>
     * </ol>
     * @param sep The separator used to separate the fields.
     * @return A string containing six fields delimited by {@code sep}.
     */
    public String getFormattedString(String sep) {
        if (sep == null || sep.length() == 0) {
            sep = ";";
        }
        StringBuilder result = new StringBuilder();
        for (TrainRide ride : rides) {
            result.append(ride.getVertrektijd() + sep);
            RideDuration deptDelay = ride.getVertrektijdVertraging();
            result.append(((deptDelay == null) ? "00:00" : deptDelay) + sep);
            result.append(ride.getAankomsttijd() + sep);
            RideDuration arrDelay = ride.getAankomsttijdVertraging();
            result.append(((arrDelay == null) ? "00:00" : arrDelay) + sep);
            result.append(ride.getReistijd() + sep);
            result.append(ride.getReistijdActueel() + "\n");
        }
        return result.toString();
    }

    public String getFormattedString() {
        return getFormattedString(";");
    }

    public JsonObject getJson() {
        return json;
    }

    public List<TrainRide> getRides() {
        return rides;
    }

    public ResponseLocation getDepartureLocation() {
        return departureLocation;
    }

    public ResponseLocation getArrivalLocation() {
        return arrivalLocation;
    }

    public String getNowIso8601Date() {
        return nowIso8601Date;
    }
}
