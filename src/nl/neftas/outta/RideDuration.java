package nl.neftas.outta;

public class RideDuration {
    private int uren;
    private int minuten;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        RideDuration rideDuration = (RideDuration) o;

        if (uren != rideDuration.uren) return false;
        return minuten == rideDuration.minuten;
    }

    @Override
    public int hashCode() {
        return 31 * uren + minuten;
    }

    @Override
    public String toString() {
        // uren en minuten are ints, so no need to check for null
        return uren + ":" + minuten;
    }

    public RideDuration(int uren, int minuten) {
        this.uren = uren;
        this.minuten = minuten;
    }

    public int getUren() {
        return uren;
    }

    public int getMinuten() {
        return minuten;
    }
}
