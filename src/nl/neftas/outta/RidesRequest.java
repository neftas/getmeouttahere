package nl.neftas.outta;

import java.io.*;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Properties;
import java.util.zip.GZIPInputStream;

public class RidesRequest implements Request {
    private TravelInformation travelInformation;
    private String strUrl;
    private String response;
    private Properties outtaProperties;

    public RidesRequest(TravelInformation travelInformation) {
        this.travelInformation = travelInformation;
        String departure = null;
        String destination = null;
        try {
            departure = (travelInformation.isDepartureLocationNull()) ? "TREINSTATION-" + travelInformation.getDepartureNameURLEncoded() : travelInformation.getDepartureLocation().getFormatted();
            destination = (travelInformation.isArrivalLocationNull()) ? "TREINSTATION-" + travelInformation.getArrivalNameURLEncoded() : travelInformation.getArrivalLocation().getFormatted();
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace(System.err);
            System.exit(1);
        }
        final String outtaPropFilename = "outta.properties";
        outtaProperties = new Properties();

        try (InputStreamReader isr = new InputStreamReader(getClass().getClassLoader().getResourceAsStream(outtaPropFilename))) {
            // outtaProperties.load(getClass().getClassLoader().getResourceAsStream(outtaPropFilename));
            outtaProperties.load(isr);
        } catch (IOException e) {
            e.printStackTrace(System.err);
            System.exit(1);
        }
        final String requestURL = outtaProperties.getProperty("RidesRequestURL");
        if (requestURL == null) {
            throw new NullPointerException("Property 'RidesRequestURL' not found");
        }
        this.strUrl = String.format(requestURL, destination, travelInformation.getDateTime(), (travelInformation.useArrivalTime()) ? "AANKOMST" : "VERTREK", departure);
    }

    // create static factory methods that return objects
    // - different departure and arrival train stations
    // - different time / day

    @Override
    public void doRequest() {
        URL url = null;
        try {
            url = new URL(strUrl);
        } catch (MalformedURLException ex) {
            ex.printStackTrace();
            System.exit(2);
        }
        StringBuilder result = new StringBuilder(5000);
        HttpURLConnection conn = null;
        try {
            conn = (HttpURLConnection) url.openConnection();
            conn.setConnectTimeout(Integer.parseInt(outtaProperties.getProperty("requestTimeout")));
            conn.setRequestProperty("Accept", "application/json, text/plain, */*");
            conn.setRequestProperty("Accept-Encoding", "gzip, deflate");
            conn.setRequestProperty("DNT", "1");
            conn.setRequestProperty("Host", outtaProperties.getProperty("Host"));
            conn.setRequestProperty("Referer", outtaProperties.getProperty("Referer"));
            GZIPInputStream gzipResponse = new GZIPInputStream(conn.getInputStream());
            BufferedReader br = new BufferedReader(new InputStreamReader(gzipResponse));
            String nextLine;
            while ((nextLine = br.readLine()) != null) {
                result.append(nextLine);
            }
            br.close();
        } catch (IOException ex) {
            ex.printStackTrace();
            System.exit(3);
        } finally {
            if (conn != null) {
                conn.disconnect();
            }
        }
        response = result.toString();
    }

    public String getResponse() {
        return response;
    }

    public TravelInformation getTravelInformation() {
        return travelInformation;
    }
}
