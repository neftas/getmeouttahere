package nl.neftas.outta;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;

public class Location {
    private String name;
    private String type;

    private Location(String name, String type) {
        this.name = name;
        this.type = type;
    }

    public static Location newInstance(String name, String type) {
        if (name == null || type == null) {
            throw new IllegalArgumentException("Argument cannot be null");
        }
        return new Location(name, type);
    }

    @Override
    public String toString() {
        return "Location{" +
                "name='" + name + '\'' +
                ", type='" + type + '\'' +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Location location = (Location) o;

        if (!name.equals(location.name)) return false;
        return type.equals(location.type);
    }

    @Override
    public int hashCode() {
        int result = name.hashCode();
        result = 31 * result + type.hashCode();
        return result;
    }

    public String getName() throws UnsupportedEncodingException {
        return URLEncoder.encode(name, "UTF-8");
    }

    public String getFormatted() throws UnsupportedEncodingException {
        return type + "-" + URLEncoder.encode(name, "UTF-8");
    }

    public String getType() {
        return type;
    }
}
