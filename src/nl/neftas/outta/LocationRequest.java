package nl.neftas.outta;

public class LocationRequest implements Request {
    private TravelInformation travelInformation;
    private String response;

    public LocationRequest(TravelInformation travelInformation) {
        this.travelInformation = travelInformation;
    }

    @Override
    public void doRequest() {
        throw new UnsupportedOperationException();
    }

    public String getResponse() {
        return response;
    }

    public TravelInformation getTravelInformation() {
        return travelInformation;
    }
}
