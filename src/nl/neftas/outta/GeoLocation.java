package nl.neftas.outta;

public class GeoLocation {
    private double breedtegraad;
    private double lengtegraad;

    public GeoLocation(double breedtegraad, double lengtegraad) {
        this.breedtegraad = breedtegraad;
        this.lengtegraad = lengtegraad;
    }

    @Override
    public String toString() {
        return "GeoLocation{" +
                "breedtegraad=" + breedtegraad +
                ", lengtegraad=" + lengtegraad +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        GeoLocation that = (GeoLocation) o;

        if (Double.compare(that.breedtegraad, breedtegraad) != 0) return false;
        return Double.compare(that.lengtegraad, lengtegraad) == 0;
    }

    @Override
    public int hashCode() {
        int result;
        long temp;
        temp = Double.doubleToLongBits(breedtegraad);
        result = (int) (temp ^ (temp >>> 32));
        temp = Double.doubleToLongBits(lengtegraad);
        result = 31 * result + (int) (temp ^ (temp >>> 32));
        return result;
    }
}
