package nl.neftas.outta;

public class Activity {
    private String id;
    private int versie;
    private String titel;
    private String subTitel;
    private String advies;
    private String gevolg;
    private String type;
    private String oorzaak;
    private String extraReistijd;

    public Activity(String id, int versie, String titel, String subTitel, String advies, String gevolg, String type, String oorzaak, String extraReistijd) {
        this.id = id;
        this.versie = versie;
        this.titel = titel;
        this.subTitel = subTitel;
        this.advies = advies;
        this.gevolg = gevolg;
        this.type = type;
        this.oorzaak = oorzaak;
        this.extraReistijd = extraReistijd;
    }

    @Override
    public String toString() {
        return "Activity{" +
                "id='" + id + '\'' +
                ", versie=" + versie +
                ", titel='" + titel + '\'' +
                ", subTitel='" + subTitel + '\'' +
                ", advies='" + advies + '\'' +
                ", gevolg='" + gevolg + '\'' +
                ", type='" + type + '\'' +
                ", oorzaak='" + oorzaak + '\'' +
                ", extraReistijd='" + extraReistijd + '\'' +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Activity that = (Activity) o;

        if (versie != that.versie) return false;
        return id.equals(that.id);
    }

    @Override
    public int hashCode() {
        int result = id.hashCode();
        result = 31 * result + versie;
        return result;
    }

    public String getId() {
        return id;
    }

    public int getVersie() {
        return versie;
    }

    public String getTitel() {
        return titel;
    }

    public String getSubTitel() {
        return subTitel;
    }

    public String getAdvies() {
        return advies;
    }

    public String getGevolg() {
        return gevolg;
    }

    public String getType() {
        return type;
    }

    public String getOorzaak() {
        return oorzaak;
    }

    public String getExtraReistijd() {
        return extraReistijd;
    }
}
