package nl.neftas.outta;

import org.jsoup.Jsoup;

public class Disturbance {
    private String id;
    private int meldtijd;
    private int aanmaakTijd;
    private int versie;
    private String titel;
    private String bericht;
    private boolean landelijk;

    public Disturbance(String id, int meldtijd, int aanmaakTijd, int versie, String titel, String bericht, boolean landelijk) {
        this.id = id;
        this.meldtijd = meldtijd;
        this.aanmaakTijd = aanmaakTijd;
        this.versie = versie;
        this.titel = titel;
        this.bericht = bericht;
        this.landelijk = landelijk;
    }

    @Override
    public String toString() {
        return "Disturbance{" +
                "id='" + id + '\'' +
                ", meldtijd=" + meldtijd +
                ", aanmaakTijd=" + aanmaakTijd +
                ", versie='" + versie + '\'' +
                ", titel='" + titel + '\'' +
                ", bericht='" + bericht + '\'' +
                ", landelijk=" + landelijk +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Disturbance that = (Disturbance) o;

        return id.equals(that.id);
    }

    @Override
    public int hashCode() {
        return id.hashCode();
    }

    public String getId() {
        return id;
    }

    public int getMeldtijd() {
        return meldtijd;
    }

    public int getAanmaakTijd() {
        return aanmaakTijd;
    }

    public int getVersie() {
        return versie;
    }

    public String getTitel() {
        return titel;
    }

    public String getBericht() {
        return bericht;
    }

    public String getBerichtWithoutHTML() {
        return Jsoup.parse(bericht).text();
    }

    public boolean isLandelijk() {
        return landelijk;
    }
}
