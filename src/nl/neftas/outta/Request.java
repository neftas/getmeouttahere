package nl.neftas.outta;

public interface Request {
    public void doRequest();
    public String getResponse();
}
