package nl.neftas.outta;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.util.Calendar;
import java.util.TimeZone;

public class TravelInformation {
    private static final TimeZone CEST = TimeZone.getTimeZone("Europe/Paris");
    private String departureName;
    private Location departureLocation;
    private boolean isDepartureLocationNull = true;
    private String arrivalName;
    private Location arrivalLocation;
    private boolean isArrivalLocationNull = true;
    private String dateTime;
    private boolean useArrivalTime;
    private LocationRequest locationRequest;

    public TravelInformation(String departureStation, String arrivalStation, String dateTime, boolean useArrivalTime) {
        this.departureName = departureStation;
        this.arrivalName = arrivalStation;
        this.dateTime = (dateTime == null) ? String.format("%tFT%<tR", Calendar.getInstance(CEST)) : dateTime;
        this.useArrivalTime = useArrivalTime;
    }

    public void searchLocations() {
        throw new UnsupportedOperationException("This method is not yet implemented");
    }

    @Override
    public String toString() {
        return "TravelInformation{" +
                "departureName='" + departureName + '\'' +
                ", arrivalName='" + arrivalName + '\'' +
                ", dateTime='" + dateTime + '\'' +
                ", useArrivalTime=" + useArrivalTime +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        TravelInformation that = (TravelInformation) o;

        if (useArrivalTime != that.useArrivalTime) return false;
        if (departureName != null ? !departureName.equals(that.departureName) : that.departureName != null)
            return false;
        if (arrivalName != null ? !arrivalName.equals(that.arrivalName) : that.arrivalName != null)
            return false;
        return dateTime != null ? dateTime.equals(that.dateTime) : that.dateTime == null;
    }

    @Override
    public int hashCode() {
        int result = departureName != null ? departureName.hashCode() : 0;
        result = 31 * result + (arrivalName != null ? arrivalName.hashCode() : 0);
        result = 31 * result + (dateTime != null ? dateTime.hashCode() : 0);
        result = 31 * result + (useArrivalTime ? 1 : 0);
        return result;
    }

    public String getDepartureName() {
        return departureName;
    }

    public String getDepartureNameURLEncoded() throws UnsupportedEncodingException {
        return URLEncoder.encode(departureName, StandardCharsets.UTF_8.name());
    }

    public void setDepartureName(String departureName) {
        this.departureName = departureName;
    }

    public String getArrivalName() {
        return arrivalName;
    }

    public String getArrivalNameURLEncoded() throws UnsupportedEncodingException {
        return URLEncoder.encode(arrivalName, StandardCharsets.UTF_8.name());
    }

    public void setArrivalName(String arrivalName) {
        this.arrivalName = arrivalName;
    }

    public String getDateTime() {
        return dateTime;
    }

    public void setDateTime(String dateTime) {
        this.dateTime = dateTime;
    }

    public boolean useArrivalTime() {
        return useArrivalTime;
    }

    public void setUseArrivalTime(boolean useArrivalTime) {
        this.useArrivalTime = useArrivalTime;
    }

    public Location getDepartureLocation() {
        return departureLocation;
    }

    public boolean isDepartureLocationNull() {
        return isDepartureLocationNull;
    }

    public Location getArrivalLocation() {
        return arrivalLocation;
    }

    public boolean isArrivalLocationNull() {
        return isArrivalLocationNull;
    }

    public LocationRequest getLocationRequest() {
        return locationRequest;
    }
}
