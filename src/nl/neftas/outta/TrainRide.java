package nl.neftas.outta;

public class TrainRide {
    private String vertrektijd;
    private String aankomsttijd;
    private RideDuration reistijd;
    private RideDuration reistijdActueel;
    private RideDuration vertrektijdVertraging;
    private RideDuration aankomsttijdVertraging;
    private String status;
    private int aantalOverstappen;
    private int hash;

    public TrainRide(String vertrektijd, String aankomsttijd, RideDuration reistijd, RideDuration reistijdActueel, RideDuration vertrektijdVertraging, RideDuration aankomsttijdVertraging, String status, int aantalOverstappen, int hash) {
        this.vertrektijd = vertrektijd;
        this.aankomsttijd = aankomsttijd;
        this.reistijd = reistijd;
        this.reistijdActueel = reistijdActueel;
        this.vertrektijdVertraging = vertrektijdVertraging;
        this.aankomsttijdVertraging = aankomsttijdVertraging;
        this.status = status;
        this.aantalOverstappen = aantalOverstappen;
        this.hash = hash;
    }

    public String getAankomsttijd() {
        return aankomsttijd;
    }

    public String getVertrektijd() {
        return vertrektijd;
    }

    @Override
    public String toString() {
        return "TrainRide{" +
                "vertrektijd='" + vertrektijd + '\'' +
                ", aankomsttijd='" + aankomsttijd + '\'' +
                ", reistijd=" + reistijd +
                ", reistijdActueel=" + reistijdActueel +
                ", vertrektijdVertraging=" + vertrektijdVertraging +
                ", aankomsttijdVertraging=" + aankomsttijdVertraging +
                ", status='" + status + '\'' +
                ", aantalOverstappen=" + aantalOverstappen +
                ", hash=" + hash +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof TrainRide)) return false;

        TrainRide trainRide = (TrainRide) o;

        if (hash != trainRide.hash) return false;
        if (!vertrektijd.equals(trainRide.vertrektijd)) return false;
        return aankomsttijd.equals(trainRide.aankomsttijd);
    }

    @Override
    public int hashCode() {
        int result = vertrektijd.hashCode();
        result = 31 * result + aankomsttijd.hashCode();
        result = 31 * result + hash;
        return result;
    }

    public int getHash() {
        return hash;
    }

    public RideDuration getReistijd() {
        return reistijd;
    }

    public RideDuration getReistijdActueel() {
        return reistijdActueel;
    }

    public RideDuration getVertrektijdVertraging() {
        return vertrektijdVertraging;
    }

    public RideDuration getAankomsttijdVertraging() {
        return aankomsttijdVertraging;
    }

    public String getStatus() {
        return status;
    }

    public int getAantalOverstappen() {
        return aantalOverstappen;
    }

}
