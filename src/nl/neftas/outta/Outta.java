package nl.neftas.outta;

import org.apache.commons.cli.*;

import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Properties;

public class Outta {
    public static final String FROM = "from";
    public static final String TO = "to";
    public static final String ARRIVAL = "arrival";
    public static final String DATE = "date";
    public static final String PRETTY = "pretty";

    private static Options cliOptions = new Options();
    private CommandLine commandLine;

    public static final String HELP = "help";
    public static final String PASSTHROUGH = "passthrough";

    public static final String SEPARATOR = "separator";

    static {
        cliOptions.addOption("f", FROM, true, "The departure station.");
        cliOptions.addOption("t", TO, true, "The arrival station.");
        cliOptions.addOption("d", DATE, true, "The date and time in yyyy-MM-ddTHH:mm format.");
        cliOptions.addOption("a", ARRIVAL, false, "Date and time specified is arrival time (default: departure time).");
        cliOptions.addOption("h", HELP, false, "Show this help text.");
        cliOptions.addOption(Option.builder()
                                   .longOpt(PRETTY)
                                   .desc("Format result in a pretty way.")
                                   .hasArg(false)
                                   .build());
        cliOptions.addOption(Option.builder()
                                   .longOpt(PASSTHROUGH)
                                   .desc("Return response from web service directly without parsing. Returns a (long) JSON string.")
                                   .hasArg(false)
                                   .build());
        cliOptions.addOption("F", SEPARATOR, true, "Separates result fields.");
    }

    public static void main(String[] args) {
        new Outta().start(args);
    }

    public void start(String[] args) {
        TravelInformation travelInformation = parseCommandLineParameters(args);
        RidesRequest ridesRequest = new RidesRequest(travelInformation);
        ridesRequest.doRequest();
        if (commandLine.hasOption(PASSTHROUGH)) {
            System.out.println(ridesRequest.getResponse());
            System.exit(0);
        }
        Response response = new Response(ridesRequest.getResponse());
        if (commandLine.hasOption(PRETTY)) {
            System.out.println(response.getPretty());
        } else if (commandLine.hasOption(SEPARATOR)) {
            String sep = commandLine.getOptionValue(SEPARATOR);
            if (sep == null || sep.length() == 0) {
                usage();
                System.exit(1);
            }
            // TODO (neftas): correct method name?
            System.out.print(response.getFormattedString(sep));
        } else {
            System.out.print(response.getFormattedString());
        }
    }

    private TravelInformation parseCommandLineParameters(String[] args) {
        CommandLineParser cliParser = new DefaultParser();
        try {
            commandLine = cliParser.parse(cliOptions, args, false);
        } catch (ParseException e) {
            System.err.println(e.getMessage());
            usage();
            e.printStackTrace(System.err);
            System.exit(1);
        }

        // TODO (neftas): read default values from properties file
        if (commandLine.hasOption("help")) {
            usage();
            System.exit(0);
        }

        final String outtaPropFilename = "outta.properties";
        Properties outtaProperties = new Properties();

        try (InputStreamReader isr = new InputStreamReader(getClass().getClassLoader().getResourceAsStream(outtaPropFilename))) {
            outtaProperties.load(isr);
        } catch (IOException e) {
            e.printStackTrace(System.err);
            System.exit(1);
        }
        String departureStation = commandLine.getOptionValue(FROM, outtaProperties.getProperty("defaultDeparture"));
        String arrivalStation = commandLine.getOptionValue(TO, outtaProperties.getProperty("defaultArrival"));
        String dateTime = commandLine.getOptionValue(DATE);
        boolean isArrivalTime = commandLine.hasOption(ARRIVAL);
        return new TravelInformation(departureStation, arrivalStation, dateTime, isArrivalTime);
    }

    public static void usage() {
        HelpFormatter helpFormatter = new HelpFormatter();
        final String commandLineSyntax =
                "getmeouttahere [-f|--from location] [-t|--to location]\t\t[-d|--date datetime] [-a|--arrival]";
        helpFormatter.printHelp(80, commandLineSyntax, "\nOPTIONS", cliOptions, "");
    }

}
