package nl.neftas.outta;

public class ResponseLocation {
    private String id;
    private String naam;
    private GeoLocation geoLocatie;
    private String landcode;
    private String stationscode;
    private String locatietype;
    private boolean buitenlands;

    public ResponseLocation(String id, String naam, GeoLocation geoLocatie, String landcode, String stationscode, String locatietype, boolean buitenlands) {
        this.id = id;
        this.naam = naam;
        this.geoLocatie = geoLocatie;
        this.landcode = landcode;
        this.stationscode = stationscode;
        this.locatietype = locatietype;
        this.buitenlands = buitenlands;
    }

    @Override
    public String toString() {
        return "ResponseLocation{" +
                "id='" + id + '\'' +
                ", naam='" + naam + '\'' +
                ", geoLocatie=" + geoLocatie +
                ", landcode='" + landcode + '\'' +
                ", stationscode='" + stationscode + '\'' +
                ", locatietype='" + locatietype + '\'' +
                ", buitenlands=" + buitenlands +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        ResponseLocation responseLocation = (ResponseLocation) o;

        return id.equals(responseLocation.id);
    }

    @Override
    public int hashCode() {
        return id.hashCode();
    }

    public String getId() {
        return id;
    }

    public String getNaam() {
        return naam;
    }

    public GeoLocation getGeoLocatie() {
        return geoLocatie;
    }

    public String getLandcode() {
        return landcode;
    }

    public String getStationscode() {
        return stationscode;
    }

    public String getLocatietype() {
        return locatietype;
    }

    public boolean isBuitenlands() {
        return buitenlands;
    }
}
