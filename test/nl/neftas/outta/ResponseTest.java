package nl.neftas.outta;

import org.junit.Before;
import org.junit.Test;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;

import static org.junit.Assert.assertEquals;

public class ResponseTest {

    private Response response;

    @Before
    public void setUp() {
        String homeDir = System.getProperty("user.home");
        String sep = File.separator;
        File responseFile = new File(homeDir + sep + "IdeaProjects" + sep + "GetMeOuttaHere" + sep + "test" + sep + "resources" + sep + "response_disturbances.json");
        if (!responseFile.exists()) {
            System.err.println(String.format("File '%s' does not exist.", responseFile.toString()));
            System.exit(1);
        }
        try {
            String jsonStr = new String(Files.readAllBytes(responseFile.toPath()));
            response = new Response(jsonStr);
            System.out.println(response.getPretty());
        } catch (IOException ex) {
            ex.printStackTrace();
            System.exit(2);
        }
    }

    @Test
    public void testResponseNumberOfRidesShouldBeFive() {
        // act
        int expected = 5;
        int actual = response.getRides().size();
        // assert
        assertEquals(expected, actual);
    }
}
